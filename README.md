# Ludum Dare 34 (2015) - Wayward Souls

Wayward Souls is a story-driven 2D platformer originally written for a 48 hour game competition [Ludum Dare 34](http://ludumdare.com/compo/ludum-dare-34/?action=preview&uid=55548). The theme for this competition was *Growing* and/or *Two Button Controls*. Wayward Souls is based on the *Growing* theme. 


## Building the Game

		python -m venv _env
		source ./_env/bin/activate
		pip install -r requirements.txt
		pyinstaller Program.py


## Running the Game

		python Program.py


## Controls

    A - Left 
    S - Crouch 
    D - Right 
    Space - Jump 
    ESC - Quit 

### Details

## Created with

    Art - GIMP 
    Audio - audacity 
    Programming Language - Python using pygame (SDL) library & pyinstaller 
    IDE - Pycharm 

## Sounds

    Wall breaking - rapid succession of making explosion noises with mouth 
    Grunt - imitating an animal grunt 
    Eating - actual eating of cereal sped up 2x 
